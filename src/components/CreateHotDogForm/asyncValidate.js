const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))
const asyncValidate = (values /*, dispatch */) => sleep(1000).then(async () => {
  const hds = JSON.parse(window.sessionStorage.getItem('htds'));
  const htdsns = hds.map(a => a.name);
  if (htdsns.includes(values.name)) {
    throw ({ name: 'That name already exist' })
  }
});

export default asyncValidate;