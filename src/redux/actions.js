export const GET_HOTDOGS = 'GET_HOTDOGS';
export const EDIT_HOTDOG = 'EDIT_HOTDOG';

export function editHotdog(data) {
  return {
    type: EDIT_HOTDOG,
    payload: data,
  };
}

export const getHotDogs = () => {
   return (dispatch) => {
    return fetch('https://hotcrud.herokuapp.com/get', {method: 'get'})
      .then((response) => response.json())
      .then((data) => {
        dispatch({ type: 'GET_HOTDOGS', payload: data });
        window.sessionStorage.setItem('htds', JSON.stringify(data));
      });
  };
};
